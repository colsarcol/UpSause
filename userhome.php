<?php
	session_start();
	if(!isset($_SESSION['username']))
	{
		header("Location:index.php");
	}
?>
<html>
	<head>
		<title>BitBiscuit</title>
		<style>
			#header {
				overflow auto;
				height: 100px;
				display: flex;
				flex-direction: column;
				background: #FF8200;
				color: #FFFFFF;
				border: 10px solid #58595b;
				padding-left: 1em;
				padding-right: 1em;
			}

			#body {
				padding-left: 1em;
				padding-right: 1em;
				overflow: auto;
				border: 10px solid #58595b;
				color: #58595b;
				height: 100%;
				margin-bottom: 10px;
				margin-top: 10px;
			}

			p {
			padding-left: 1em;
			padding-right; 1em;
			font-size: 1.5em;;
			}
			
		</style>
		<style>
			.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '+1';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 34px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}
		</style>
				<style>
			.button2 {
  display: inline-block;
  border-radius: 4px;
  background-color: #f4511e;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button2 span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button2 span:after {
  content: '>';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button2:hover span {
  padding-right: 30px;
}

.button2:hover span:after {
  opacity: 1;
  right: 0;
}
		</style>
		
	</head>
	<body>
	
		<div id = "header"><h1><center> User Home</h1></center> </div>
		<div id = "body">
			<?php
				echo "Hello " . $_SESSION['username'] . ". <br>";
			?>
			<p>Do you want to add a bite to your count or do you want to view all bites?
				<br>	
				<br>
					<!--Button when clicked adds a point to the php script-->
					<center><form action="add.php" method="get">
						<button type="submit" class="button" style="vertical-align:middle"><span>Add Bite</span></button>
					</form></center>
				<br>
				<br>
					<!--Button when clicked should call a php funtion to show all users and graphs-->
					<center><button class="button2" style="vertical-align:middle"><span>View Bites</span></button></center>
			</p>
		</div>
			
		</div>
	</body>
</html>
